FROM alpine:3.10.1

RUN chmod 777 /home
ENV HOME=/home
RUN apk update && apk add bash

RUN apk add openjdk7
ENV _JAVA_OPTIONS=-Duser.home=/home
ENV JAVA_HOME=/usr/lib/jvm/java-1.7-openjdk
